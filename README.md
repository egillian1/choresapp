# ChoresApp

App with chore rota

## Setup and development
App is best used with `rack`, which in turn requires `ruby`. Install `ruby` for your system and then navigate to the `site/` directory. Run `bundle install` and then run `rackup`. This should set up a local web server to hos the project for you.

## Pushing to Heroku
Git commit as normal to `master` and then deploy from master to heroku using `git push heroku master`. View what the heroku deployment looks like with `heroku open`.

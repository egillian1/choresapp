const weekLength = 604800;
const people = [
    "Miguel",
    "Rich",
    "Tom",
    "Gudny",
    "Ian"
];
const numPeople = people.length;
const tasks = [
    "Freebird",
    "Bathroom",
    "Kitchen",
    "Vacuum",
    "Garbej"
];
const taskDescriptions = [
    "Feels like you're doing nothing at all! (nothing at all)",
    "Bathroom cleaning: - Scrub shower with limescale remover - Remove blockage from shower drain - Clean sink and scrub faucet - Clean toilet with toilet brush - Change towels and bathmat (if necessary) - Restock toilet paper - Remove garbage from trash can",
    "Kitchen cleaning: - Remove grease from around hobs and on wall - Wipe down all cooking surfaces - Clean sink, faucet and area under dish rack - Clean microwave (if necessary) - Wipe down cupboards and washing machines (if necessary)",
    "Vacuum cleaning: - Vacuum the kitchen, downstairs hallway, living room and rug, staircase, upstairs hallway, and bathroom (if dry) - Empty vacuum canister into trash",
    "Trash man: - Remove garbage bags, compost bags and recycling when full - Take the appropriate bins to the curb on Wednesdays (collection dates at https://online.bournemouth.gov.uk/services/bindaylookup/ )"
];

let startDate = new Date("September 17, 2018 00:00:00").getTime() / 1000;
let now = Date.now() / 1000;

let timePassedFromStart = now - startDate;
let weeksPassedFromStart = timePassedFromStart / weekLength;
let weekOffset = Math.floor(weeksPassedFromStart % numPeople);


let tasksThisWeek = [];

for(var i = 0; i < tasks.length; i++) {
    let pointer = (i + weekOffset) % numPeople;
    tasksThisWeek.push({
        "task": tasks[pointer],
        "description": taskDescriptions[pointer]
    });
}

console.log("now and startDate", now, startDate);
console.log("weekOffset", weekOffset);
console.log("tasks this week", tasksThisWeek);

let addDescriptionClickHandlers = function(element, description) {
    console.log("adding stuff to", element, description);
    let separateLines = description.split(" - ");
    console.log("separate", separateLines);
    element.onclick = function() {
        let container = document.getElementById('description');
        container.innerHTML = "";
        for(var i = 0; i < separateLines.length; i++) {
            let line = document.createElement('div');
            let prefix = "";
            if(i != 0 ) {
                prefix = " - ";
            }
            line.textContent = prefix + separateLines[i];
            container.appendChild(line);
        }
        container.classList.remove('hide');
    }
}

document.addEventListener("DOMContentLoaded", function() {
    let wrapper = document.getElementsByClassName("wrapper")[0];
    for(var i = 0; i < numPeople; i++) {
        let row = document.createElement('div');
        row.classList.add("row");

        let nameElement = document.createElement('div');
        nameElement.classList.add("name");
        nameElement.textContent = people[i];

        let taskElement = document.createElement('div');
        taskElement.classList.add("task");
        taskElement.textContent = tasksThisWeek[i].task;
        addDescriptionClickHandlers(taskElement, tasksThisWeek[i].description);

        row.appendChild(nameElement);
        row.appendChild(taskElement);
        wrapper.appendChild(row);
    }

    let description = document.getElementById('description');
    description.onclick = function() {
        this.classList.add('hide');
    };  
});
